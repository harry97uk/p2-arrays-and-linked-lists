//
//  LinkedList.hpp
//  CommandLineTool
//
//  Created by Harry Gardiner on 09/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#pragma once

#include <stdio.h>

class LinkedList
{
public:
    
    LinkedList();
    
    ~LinkedList();
    
    void add (float itemValue);
    
    float get (int index) const;
    
    int size() const;
    
private:
    
    struct Node
    {
        float value;
        Node* next;
    };
    Node* head;
    Node* conductor;
};


