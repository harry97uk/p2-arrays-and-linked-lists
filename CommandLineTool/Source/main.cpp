//
//  main.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 08/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include <iostream>
#include "Array.hpp"
#include "LinkedList.hpp"
int main()
{
    
    Array value;
    std::cout << "value = " << value.get(0);
    std::cout << "\nsize = " << value.getSize();
    
    LinkedList list;
    list.add(3);
    list.add(7);
    std::cout << "\nvalue = " << list.get(1);
    
    return 0;
}
