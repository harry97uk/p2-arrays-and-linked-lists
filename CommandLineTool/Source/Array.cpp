//
//  Array.cpp
//  CommandLineTool
//
//  Created by Harry Gardiner on 09/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include "Array.hpp"

Array::Array()
{
    size = 0;
    array = new float[size];
    
}

Array::~Array()
{
    delete [] array;
}

void Array::add(float newValue)
{
    
    float* tempArray = new float[size + 1];
    
    for(int count = 0; count < size; count++)
    {
        tempArray[count] = array[count];
    }
    
    tempArray[size] = newValue;
    
    delete [] array;
    
    array = tempArray;
    
    size = size + 1;
        
}

float Array::get(int index) const
{
    return array[index];
}

int Array::getSize() const
{
    return size;
}

bool Array::testArray()
{
    bool arraySize = true;
    int counter;
    
    for (counter = 0; counter<=size; counter++) {
        if (size == counter) {
            arraySize = true;
        }
        else {
            arraySize = false;
        }
    }
   
    return arraySize;
    
    
}
