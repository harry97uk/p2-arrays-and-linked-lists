//
//  LinkedList.cpp
//  CommandLineTool
//
//  Created by Harry Gardiner on 09/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include "LinkedList.hpp"
#include <iostream>
LinkedList::LinkedList()
{
    
}

LinkedList::~LinkedList()
{
    
}

void LinkedList::add(float itemValue)
{
    head = new Node;
    head->next = nullptr;
    head->value = itemValue;
    
    conductor = head;
    if (conductor != 0) {
        while (conductor->next != 0) {
            conductor = conductor->next;
        }
        conductor->next = new Node;
        conductor = conductor->next;
        conductor->next = 0;
        conductor->value = itemValue;
    }
}

float LinkedList::get(int index) const
{
    Node* tempnode;
    int count = 0;
    
    tempnode = head;
    int number = size();
    
    if (tempnode == nullptr) {
        std::cout << "The List is Empty";
    }
    else {
        if ((index <= 0) && (index < number)) {
            while (tempnode != nullptr) {
                if (count == index) {
                     return tempnode->value;
                }
                count++;
                tempnode = tempnode->next;
            }
        }
    }
    
}

int LinkedList::size() const
{
    Node* tempnode1;
    int count = 0;
    
    tempnode1 = head;
    
    if (tempnode1 == nullptr) {
        std::cout << "The List is Empty";
    }
    else {
        while (tempnode1 !=nullptr) {
            count++;
            tempnode1 = tempnode1->next;
        }
            return count;
        }
    }

