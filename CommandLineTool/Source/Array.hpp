//
//  Array.hpp
//  CommandLineTool
//
//  Created by Harry Gardiner on 09/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#pragma once

#include <stdio.h>
#include <iostream>

class Array
{
public:
    Array();
    
    ~Array();
    
    void add(float newValue);
    
    float get(int index) const;
    
    int getSize() const;
    
    bool testArray();
    

private:
        int size;
        float* array = nullptr;
    
};


